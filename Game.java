package QuickHit;

import java.util.Random;

/*
 * 游戏类
 * 
 */
public class Game {
	
	private Player player;//玩家
	/*
	 * 构造方法，传入玩家信息。
	 * @param payer玩家
	 */
	public Game(Player player)
	{
		this.player=player;
	}
	

	/*
	 * 输出指定级别规定长度的字符串。
	 * @return 输出的字符串，用于和用户输入比较
	 */
	public String printStr()
	{
		int strLength=LevelParam.levels[player.getLevelNo()-1].getStrLength();
		StringBuffer buffer=new StringBuffer();
		//通过循环生成需要输出的字符串
		Random random=new Random();
		for(int i=0;i<strLength;i++)
		{
			int rand=random.nextInt(strLength);
			switch(rand)
			{
			case 0:
				buffer.append("w");
				break;
			case 1:
				buffer.append("a");
				break;
			case 2:
				buffer.append("s");
				break;
			case 3:
				buffer.append("d");
				break;
			case 4:
				buffer.append("q");
				break;
			case 5:
				buffer.append("e");
				break;
				
			}
		}
		//输出字符串
		System.out.println(buffer);
		//返回字符串用于和玩家输入相比较
		return buffer.toString();
	}
	/*
	 * 
	 * 判断玩家输入是否正确，并输出相应结果信息。
	 * out 游戏输出的字符串
	 * in  玩家输入的字符串
	 */
	public void printResult(String out,String in)
	{
		boolean flag;
		if(out.equals(in))
		{
			flag=true;
		}
		else
		{
			flag=false;
		}
		//如果输入正确
		if(flag)
		{
			long currentTime=System.currentTimeMillis();
			if((currentTime-player.getStartTime())/1000>
			LevelParam.levels[player.getLevelNo()-1].getTimeLimit())
			{
				System.out.println("您的输入太慢了，已经超时，退出！");
				System.exit(1);
			
			//如果没有超时
			}
			else
			{
				//计算当前积分
				player.setCurScore(player.getCurScore()+
				LevelParam.levels[player.getLevelNo()-1].getPerScore());
				//计算已用时间
				player.setElapsedTime((int)((currentTime-player.getStartTime())/1000));
			    //输出当前积分、当前级别、已用时间
				System.out.println("输入正确，您的级别"+player.getLevelNo()+"，您的积分"
						+player.getCurScore()+",已用时间"+player.getElapsedTime()+"秒");
			
				//判断用户是否已经闯关最后一关
				int score = 0;
				if(player.getLevelNo()==6)
				{
					score=LevelParam.levels[player.getLevelNo()-1].getPerScore()
					*LevelParam.levels[player.getLevelNo()-1].getStrTimes();		
				}
				
				if(player.getCurScore()==score)
				{
					System.out.println("你闯关成功，成为绝顶高手，恭喜你！！！");
					System.exit(0);
				}

			}
		}//如果输入错误
		else
		{
             System.out.println("输入错误，退出！");
             System.exit(0);
		}
		
	}

}
