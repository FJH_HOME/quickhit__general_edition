package QuickHit;

public class Level {
	
	
	private int levelNo;//级别号
	private int strLength;//各级别一次输出字符串的长度
	private int strTimes;//各级别输出字符串的次数
	private int timeLimit;//各级别闯关的时间限制
	private int perScore;//各级别成功输入一次字符后增加的分数
	
	public Level(int levelNo, int strLength, int strTimes, int timeLimit, int perScore) {
		this.levelNo=levelNo;
		this.strLength=strLength;
		this.strTimes=strTimes;
		this.timeLimit=timeLimit;
		this.perScore=perScore;
	}
	
	public int getLevelNo()
	{
		return levelNo;
	}
	
	public int getStrLength()
	{
		return strLength;
	}
	
	public int getStrTimes()
	{
		return strTimes;
	}
	
	public int getTimeLimit()
	{
		return timeLimit;
	}
	
	public int getPerScore()
	{
		return perScore;
	}
	
}
